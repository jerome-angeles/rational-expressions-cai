﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Splash
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Surname = New System.Windows.Forms.TextBox()
        Me.GivenName = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.MiddleName = New System.Windows.Forms.Label()
        Me.LearnButton = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.BackButton1 = New System.Windows.Forms.Button()
        Me.StudentCredentials = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.StudentCredentials.SuspendLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.Font = New System.Drawing.Font("Molot", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(926, 92)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Rational Numbers"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Surname
        '
        Me.Surname.Font = New System.Drawing.Font("Molot", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Surname.Location = New System.Drawing.Point(16, 125)
        Me.Surname.Name = "Surname"
        Me.Surname.Size = New System.Drawing.Size(286, 35)
        Me.Surname.TabIndex = 3
        '
        'GivenName
        '
        Me.GivenName.Font = New System.Drawing.Font("Molot", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GivenName.Location = New System.Drawing.Point(308, 125)
        Me.GivenName.Name = "GivenName"
        Me.GivenName.Size = New System.Drawing.Size(286, 35)
        Me.GivenName.TabIndex = 4
        '
        'TextBox3
        '
        Me.TextBox3.Font = New System.Drawing.Font("Molot", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(600, 125)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(286, 35)
        Me.TextBox3.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(10, 90)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(130, 32)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Surname"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(302, 90)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(172, 32)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Given Name"
        '
        'MiddleName
        '
        Me.MiddleName.AutoSize = True
        Me.MiddleName.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MiddleName.Location = New System.Drawing.Point(594, 90)
        Me.MiddleName.Name = "MiddleName"
        Me.MiddleName.Size = New System.Drawing.Size(182, 32)
        Me.MiddleName.TabIndex = 8
        Me.MiddleName.Text = "Middle Name"
        '
        'LearnButton
        '
        Me.LearnButton.Font = New System.Drawing.Font("Molot", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LearnButton.Location = New System.Drawing.Point(341, 190)
        Me.LearnButton.Name = "LearnButton"
        Me.LearnButton.Size = New System.Drawing.Size(197, 77)
        Me.LearnButton.TabIndex = 9
        Me.LearnButton.Text = "Learn"
        Me.LearnButton.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Molot", 22.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(273, 22)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(350, 44)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Enter Your Name"
        '
        'BackButton1
        '
        Me.BackButton1.Font = New System.Drawing.Font("Molot", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BackButton1.Location = New System.Drawing.Point(780, 225)
        Me.BackButton1.Name = "BackButton1"
        Me.BackButton1.Size = New System.Drawing.Size(106, 42)
        Me.BackButton1.TabIndex = 11
        Me.BackButton1.Text = "Close"
        Me.BackButton1.UseVisualStyleBackColor = True
        '
        'StudentCredentials
        '
        Me.StudentCredentials.Controls.Add(Me.Label4)
        Me.StudentCredentials.Controls.Add(Me.NumericUpDown1)
        Me.StudentCredentials.Controls.Add(Me.BackButton1)
        Me.StudentCredentials.Controls.Add(Me.Surname)
        Me.StudentCredentials.Controls.Add(Me.Label5)
        Me.StudentCredentials.Controls.Add(Me.GivenName)
        Me.StudentCredentials.Controls.Add(Me.LearnButton)
        Me.StudentCredentials.Controls.Add(Me.TextBox3)
        Me.StudentCredentials.Controls.Add(Me.MiddleName)
        Me.StudentCredentials.Controls.Add(Me.Label2)
        Me.StudentCredentials.Controls.Add(Me.Label3)
        Me.StudentCredentials.Location = New System.Drawing.Point(22, 104)
        Me.StudentCredentials.Name = "StudentCredentials"
        Me.StudentCredentials.Size = New System.Drawing.Size(916, 280)
        Me.StudentCredentials.TabIndex = 12
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(10, 190)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(209, 32)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Pre-test Scores"
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.2!)
        Me.NumericUpDown1.Location = New System.Drawing.Point(20, 225)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(59, 38)
        Me.NumericUpDown1.TabIndex = 12
        '
        'Splash
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(950, 428)
        Me.Controls.Add(Me.StudentCredentials)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Splash"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form1"
        Me.StudentCredentials.ResumeLayout(False)
        Me.StudentCredentials.PerformLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Surname As System.Windows.Forms.TextBox
    Friend WithEvents GivenName As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents MiddleName As System.Windows.Forms.Label
    Friend WithEvents LearnButton As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents BackButton1 As System.Windows.Forms.Button
    Friend WithEvents StudentCredentials As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown

End Class
