﻿Public Class CAIMain

    Public Property PreTestScores As Integer
    Public Property StudentName As String
    Public Property PostTestScores As Integer = 0
    Public Property Improvement As Double

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
        Splash.Close()
    End Sub

    Private Sub AddtitionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddtitionToolStripMenuItem.Click
        Easy_Tutorial.Show()
        Easy_Tutorial.MdiParent = Me
    End Sub

    Private Sub ExitToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Splash.Close()
        Me.Close()
    End Sub

    Private Sub MediumToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MediumToolStripMenuItem.Click
        Medium_Tutorial.Show()
        Medium_Tutorial.MdiParent = Me
    End Sub

    Private Sub HardToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HardToolStripMenuItem.Click
        Hard_Tutorial.Show()
        Hard_Tutorial.MdiParent = Me
    End Sub

    Private Sub CAIMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub


    Private Sub PosttestToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PosttestToolStripMenuItem.Click
        Quiz_Main.MdiParent = Me
        Quiz_Main.Show()
    End Sub

    Private Sub ViewResultToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ViewResultToolStripMenuItem.Click
        Congratulations_Form.MdiParent = Me
        Congratulations_Form.Show()
    End Sub

    Private Sub MasterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MasterToolStripMenuItem.Click
        Master_Tutorial.MdiParent = Me
        Master_Tutorial.Show()
    End Sub
End Class