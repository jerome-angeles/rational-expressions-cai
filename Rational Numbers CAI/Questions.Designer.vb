﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Questions
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.RadioButton4 = New System.Windows.Forms.RadioButton()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.pictured = New System.Windows.Forms.PictureBox()
        Me.pictureb = New System.Windows.Forms.PictureBox()
        Me.picturec = New System.Windows.Forms.PictureBox()
        Me.picturea = New System.Windows.Forms.PictureBox()
        Me.problem = New System.Windows.Forms.PictureBox()
        CType(Me.pictured, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pictureb, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picturec, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picturea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.problem, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RadioButton4
        '
        Me.RadioButton4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadioButton4.AutoSize = True
        Me.RadioButton4.BackColor = System.Drawing.Color.Transparent
        Me.RadioButton4.Location = New System.Drawing.Point(544, 387)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(133, 33)
        Me.RadioButton4.TabIndex = 24
        Me.RadioButton4.Text = "Answer 4"
        Me.RadioButton4.UseVisualStyleBackColor = False
        '
        'RadioButton3
        '
        Me.RadioButton3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.BackColor = System.Drawing.Color.Transparent
        Me.RadioButton3.Location = New System.Drawing.Point(544, 240)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(133, 33)
        Me.RadioButton3.TabIndex = 23
        Me.RadioButton3.Text = "Answer 3"
        Me.RadioButton3.UseVisualStyleBackColor = False
        '
        'RadioButton2
        '
        Me.RadioButton2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.BackColor = System.Drawing.Color.Transparent
        Me.RadioButton2.Location = New System.Drawing.Point(136, 387)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(133, 33)
        Me.RadioButton2.TabIndex = 22
        Me.RadioButton2.Text = "Answer 2"
        Me.RadioButton2.UseVisualStyleBackColor = False
        '
        'RadioButton1
        '
        Me.RadioButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.BackColor = System.Drawing.Color.Transparent
        Me.RadioButton1.Checked = True
        Me.RadioButton1.Location = New System.Drawing.Point(136, 240)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(133, 33)
        Me.RadioButton1.TabIndex = 21
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Answer 1"
        Me.RadioButton1.UseVisualStyleBackColor = False
        '
        'Label33
        '
        Me.Label33.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label33.BackColor = System.Drawing.Color.Transparent
        Me.Label33.Location = New System.Drawing.Point(38, 77)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(955, 42)
        Me.Label33.TabIndex = 20
        Me.Label33.Text = "Add the following rational expressions"
        Me.Label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.Location = New System.Drawing.Point(460, 555)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(160, 60)
        Me.Button1.TabIndex = 30
        Me.Button1.Text = "Next"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'pictured
        '
        Me.pictured.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pictured.BackColor = System.Drawing.Color.Transparent
        Me.pictured.Image = Global.Rational_Numbers_CAI.My.Resources.Resources.prob12
        Me.pictured.Location = New System.Drawing.Point(544, 428)
        Me.pictured.Name = "pictured"
        Me.pictured.Size = New System.Drawing.Size(359, 84)
        Me.pictured.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pictured.TabIndex = 29
        Me.pictured.TabStop = False
        '
        'pictureb
        '
        Me.pictureb.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pictureb.BackColor = System.Drawing.Color.Transparent
        Me.pictureb.Image = Global.Rational_Numbers_CAI.My.Resources.Resources.prob12
        Me.pictureb.Location = New System.Drawing.Point(136, 435)
        Me.pictureb.Name = "pictureb"
        Me.pictureb.Size = New System.Drawing.Size(359, 84)
        Me.pictureb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pictureb.TabIndex = 28
        Me.pictureb.TabStop = False
        '
        'picturec
        '
        Me.picturec.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picturec.BackColor = System.Drawing.Color.Transparent
        Me.picturec.Image = Global.Rational_Numbers_CAI.My.Resources.Resources.prob12
        Me.picturec.Location = New System.Drawing.Point(544, 288)
        Me.picturec.Name = "picturec"
        Me.picturec.Size = New System.Drawing.Size(359, 84)
        Me.picturec.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picturec.TabIndex = 27
        Me.picturec.TabStop = False
        '
        'picturea
        '
        Me.picturea.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picturea.BackColor = System.Drawing.Color.Transparent
        Me.picturea.Image = Global.Rational_Numbers_CAI.My.Resources.Resources.prob12
        Me.picturea.Location = New System.Drawing.Point(136, 288)
        Me.picturea.Name = "picturea"
        Me.picturea.Size = New System.Drawing.Size(359, 84)
        Me.picturea.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picturea.TabIndex = 26
        Me.picturea.TabStop = False
        '
        'problem
        '
        Me.problem.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.problem.BackColor = System.Drawing.Color.Transparent
        Me.problem.Image = Global.Rational_Numbers_CAI.My.Resources.Resources.Problem_4
        Me.problem.Location = New System.Drawing.Point(136, 122)
        Me.problem.Name = "problem"
        Me.problem.Size = New System.Drawing.Size(767, 100)
        Me.problem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.problem.TabIndex = 25
        Me.problem.TabStop = False
        '
        'Questions
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.BackgroundImage = Global.Rational_Numbers_CAI.My.Resources.Resources.abstract_white_background_24
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1057, 640)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.pictured)
        Me.Controls.Add(Me.pictureb)
        Me.Controls.Add(Me.picturec)
        Me.Controls.Add(Me.picturea)
        Me.Controls.Add(Me.problem)
        Me.Controls.Add(Me.RadioButton4)
        Me.Controls.Add(Me.RadioButton3)
        Me.Controls.Add(Me.RadioButton2)
        Me.Controls.Add(Me.RadioButton1)
        Me.Controls.Add(Me.Label33)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(5)
        Me.Name = "Questions"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Questions"
        CType(Me.pictured, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pictureb, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picturec, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picturea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.problem, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pictured As System.Windows.Forms.PictureBox
    Friend WithEvents pictureb As System.Windows.Forms.PictureBox
    Friend WithEvents picturec As System.Windows.Forms.PictureBox
    Friend WithEvents picturea As System.Windows.Forms.PictureBox
    Friend WithEvents problem As System.Windows.Forms.PictureBox
    Friend WithEvents RadioButton4 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
