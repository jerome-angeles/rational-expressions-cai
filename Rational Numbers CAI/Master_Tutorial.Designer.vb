﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Master_Tutorial
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Einstein = New System.Windows.Forms.PictureBox()
        Me.PreviousButton = New System.Windows.Forms.Button()
        Me.NextButton = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.PictureBox16 = New System.Windows.Forms.PictureBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Splitter1 = New System.Windows.Forms.Splitter()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.PictureBox10 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.PictureBox17 = New System.Windows.Forms.PictureBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.PictureBox19 = New System.Windows.Forms.PictureBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.TabPage10 = New System.Windows.Forms.TabPage()
        Me.PictureBox15 = New System.Windows.Forms.PictureBox()
        Me.PictureBox14 = New System.Windows.Forms.PictureBox()
        Me.PictureBox13 = New System.Windows.Forms.PictureBox()
        Me.PictureBox12 = New System.Windows.Forms.PictureBox()
        Me.PictureBox11 = New System.Windows.Forms.PictureBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.RadioButton4 = New System.Windows.Forms.RadioButton()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        CType(Me.Einstein, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage6.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage7.SuspendLayout()
        CType(Me.PictureBox19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage10.SuspendLayout()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Einstein
        '
        Me.Einstein.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Einstein.BackColor = System.Drawing.Color.DimGray
        Me.Einstein.Image = Global.Rational_Numbers_CAI.My.Resources.Resources.Einstein
        Me.Einstein.Location = New System.Drawing.Point(983, 148)
        Me.Einstein.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Einstein.Name = "Einstein"
        Me.Einstein.Size = New System.Drawing.Size(267, 401)
        Me.Einstein.TabIndex = 11
        Me.Einstein.TabStop = False
        '
        'PreviousButton
        '
        Me.PreviousButton.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.PreviousButton.ForeColor = System.Drawing.Color.Black
        Me.PreviousButton.Location = New System.Drawing.Point(981, 45)
        Me.PreviousButton.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.PreviousButton.Name = "PreviousButton"
        Me.PreviousButton.Size = New System.Drawing.Size(122, 42)
        Me.PreviousButton.TabIndex = 10
        Me.PreviousButton.Text = "Previous"
        Me.PreviousButton.UseVisualStyleBackColor = True
        '
        'NextButton
        '
        Me.NextButton.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.NextButton.BackColor = System.Drawing.Color.Transparent
        Me.NextButton.ForeColor = System.Drawing.Color.Black
        Me.NextButton.Location = New System.Drawing.Point(981, 86)
        Me.NextButton.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.NextButton.Name = "NextButton"
        Me.NextButton.Size = New System.Drawing.Size(122, 42)
        Me.NextButton.TabIndex = 9
        Me.NextButton.Text = "Next"
        Me.NextButton.UseVisualStyleBackColor = False
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Controls.Add(Me.TabPage7)
        Me.TabControl1.Controls.Add(Me.TabPage10)
        Me.TabControl1.Location = New System.Drawing.Point(20, 12)
        Me.TabControl1.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(959, 711)
        Me.TabControl1.TabIndex = 8
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.DimGray
        Me.TabPage2.BackgroundImage = Global.Rational_Numbers_CAI.My.Resources.Resources.Easy_Introduction
        Me.TabPage2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.TabPage2.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 19.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage2.Location = New System.Drawing.Point(4, 38)
        Me.TabPage2.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.TabPage2.Size = New System.Drawing.Size(951, 669)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Introduction"
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.DimGray
        Me.TabPage1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.TabPage1.Controls.Add(Me.Label53)
        Me.TabPage1.Controls.Add(Me.Label52)
        Me.TabPage1.Controls.Add(Me.Label35)
        Me.TabPage1.Controls.Add(Me.PictureBox16)
        Me.TabPage1.Controls.Add(Me.Label34)
        Me.TabPage1.Controls.Add(Me.Splitter1)
        Me.TabPage1.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 19.8!)
        Me.TabPage1.Location = New System.Drawing.Point(4, 38)
        Me.TabPage1.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.TabPage1.Size = New System.Drawing.Size(951, 669)
        Me.TabPage1.TabIndex = 2
        Me.TabPage1.Text = "Example"
        '
        'Label53
        '
        Me.Label53.Location = New System.Drawing.Point(-1, 371)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(955, 42)
        Me.Label53.TabIndex = 21
        Me.Label53.Text = "This time you need to add and subtract"
        Me.Label53.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label52
        '
        Me.Label52.Location = New System.Drawing.Point(5, 53)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(955, 42)
        Me.Label52.TabIndex = 20
        Me.Label52.Text = "Though long it is very simple"
        Me.Label52.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label35
        '
        Me.Label35.ForeColor = System.Drawing.Color.LightCoral
        Me.Label35.Location = New System.Drawing.Point(3, 604)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(955, 42)
        Me.Label35.TabIndex = 19
        Me.Label35.Text = "Press next to continue"
        Me.Label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox16
        '
        Me.PictureBox16.Image = Global.Rational_Numbers_CAI.My.Resources.Resources.Problem_4
        Me.PictureBox16.Location = New System.Drawing.Point(10, 215)
        Me.PictureBox16.Name = "PictureBox16"
        Me.PictureBox16.Size = New System.Drawing.Size(936, 137)
        Me.PictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox16.TabIndex = 18
        Me.PictureBox16.TabStop = False
        '
        'Label34
        '
        Me.Label34.Location = New System.Drawing.Point(4, 0)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(955, 42)
        Me.Label34.TabIndex = 17
        Me.Label34.Text = "This time you will deal with this example"
        Me.Label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Splitter1
        '
        Me.Splitter1.Location = New System.Drawing.Point(2, 3)
        Me.Splitter1.Name = "Splitter1"
        Me.Splitter1.Size = New System.Drawing.Size(3, 663)
        Me.Splitter1.TabIndex = 16
        Me.Splitter1.TabStop = False
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.DimGray
        Me.TabPage3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.TabPage3.Controls.Add(Me.Label55)
        Me.TabPage3.Controls.Add(Me.Label54)
        Me.TabPage3.Controls.Add(Me.Label39)
        Me.TabPage3.Controls.Add(Me.PictureBox10)
        Me.TabPage3.Controls.Add(Me.PictureBox5)
        Me.TabPage3.Controls.Add(Me.Label38)
        Me.TabPage3.Controls.Add(Me.Label37)
        Me.TabPage3.Controls.Add(Me.PictureBox17)
        Me.TabPage3.Controls.Add(Me.Label36)
        Me.TabPage3.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 19.8!)
        Me.TabPage3.Location = New System.Drawing.Point(4, 38)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(951, 669)
        Me.TabPage3.TabIndex = 3
        Me.TabPage3.Text = "Step 1"
        '
        'Label55
        '
        Me.Label55.Location = New System.Drawing.Point(3, 571)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(955, 42)
        Me.Label55.TabIndex = 26
        Me.Label55.Text = "And because we now have a common denominator, we will skip steps 2"
        Me.Label55.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label54
        '
        Me.Label54.Location = New System.Drawing.Point(3, 529)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(955, 42)
        Me.Label54.TabIndex = 25
        Me.Label54.Text = "Notice that our expression already have the common denominator"
        Me.Label54.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label39
        '
        Me.Label39.Location = New System.Drawing.Point(0, 368)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(955, 42)
        Me.Label39.TabIndex = 24
        Me.Label39.Text = "And now our expression becomes this"
        Me.Label39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox10
        '
        Me.PictureBox10.Image = Global.Rational_Numbers_CAI.My.Resources.Resources.Problem_4_Step_11
        Me.PictureBox10.Location = New System.Drawing.Point(7, 413)
        Me.PictureBox10.Name = "PictureBox10"
        Me.PictureBox10.Size = New System.Drawing.Size(938, 115)
        Me.PictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox10.TabIndex = 23
        Me.PictureBox10.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = Global.Rational_Numbers_CAI.My.Resources.Resources.Problem_4_Step_1_1
        Me.PictureBox5.Location = New System.Drawing.Point(193, 294)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(579, 58)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox5.TabIndex = 22
        Me.PictureBox5.TabStop = False
        '
        'Label38
        '
        Me.Label38.ForeColor = System.Drawing.Color.LightCoral
        Me.Label38.Location = New System.Drawing.Point(-3, 614)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(955, 42)
        Me.Label38.TabIndex = 21
        Me.Label38.Text = "Press next to continue"
        Me.Label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label37
        '
        Me.Label37.Location = New System.Drawing.Point(0, 237)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(955, 42)
        Me.Label37.TabIndex = 20
        Me.Label37.Text = "This requires factoring. Since one of the denominators is a trinomial"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox17
        '
        Me.PictureBox17.Image = Global.Rational_Numbers_CAI.My.Resources.Resources.Problem_4
        Me.PictureBox17.Location = New System.Drawing.Point(7, 98)
        Me.PictureBox17.Name = "PictureBox17"
        Me.PictureBox17.Size = New System.Drawing.Size(938, 124)
        Me.PictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox17.TabIndex = 19
        Me.PictureBox17.TabStop = False
        '
        'Label36
        '
        Me.Label36.Location = New System.Drawing.Point(-3, 2)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(955, 42)
        Me.Label36.TabIndex = 1
        Me.Label36.Text = "Step 1: Factor it"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.Color.DimGray
        Me.TabPage4.Controls.Add(Me.PictureBox2)
        Me.TabPage4.Controls.Add(Me.Label5)
        Me.TabPage4.Controls.Add(Me.Label4)
        Me.TabPage4.Controls.Add(Me.Label3)
        Me.TabPage4.Controls.Add(Me.PictureBox1)
        Me.TabPage4.Controls.Add(Me.Label2)
        Me.TabPage4.Controls.Add(Me.Label1)
        Me.TabPage4.Controls.Add(Me.Label8)
        Me.TabPage4.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 19.8!)
        Me.TabPage4.Location = New System.Drawing.Point(4, 38)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(951, 669)
        Me.TabPage4.TabIndex = 13
        Me.TabPage4.Text = "Step 3"
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.Rational_Numbers_CAI.My.Resources.Resources.Problem_4_Step_4
        Me.PictureBox2.Location = New System.Drawing.Point(4, 362)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(938, 137)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 17
        Me.PictureBox2.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(436, 315)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 38)
        Me.Label5.TabIndex = 16
        Me.Label5.Text = "To"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(424, 126)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 38)
        Me.Label4.TabIndex = 15
        Me.Label4.Text = "From"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(424, 129)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(78, 38)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "From"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.Rational_Numbers_CAI.My.Resources.Resources.Problem_4_Step_1_2
        Me.PictureBox1.Location = New System.Drawing.Point(0, 170)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(938, 137)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 14
        Me.PictureBox1.TabStop = False
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(-4, 84)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(955, 42)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "The only thing we need from this step is to put the numerator together"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(-4, 42)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(955, 42)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Our expression already has a common denominator"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(-4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(955, 42)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "Step 3: Rewrite Fractions Using Common Denominator"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TabPage6
        '
        Me.TabPage6.BackColor = System.Drawing.Color.DimGray
        Me.TabPage6.Controls.Add(Me.Label21)
        Me.TabPage6.Controls.Add(Me.PictureBox7)
        Me.TabPage6.Controls.Add(Me.Label19)
        Me.TabPage6.Controls.Add(Me.PictureBox6)
        Me.TabPage6.Controls.Add(Me.Label18)
        Me.TabPage6.Controls.Add(Me.Label17)
        Me.TabPage6.Controls.Add(Me.Label16)
        Me.TabPage6.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 19.8!)
        Me.TabPage6.Location = New System.Drawing.Point(4, 38)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(951, 669)
        Me.TabPage6.TabIndex = 6
        Me.TabPage6.Text = "Step 4"
        '
        'Label21
        '
        Me.Label21.ForeColor = System.Drawing.Color.LightCoral
        Me.Label21.Location = New System.Drawing.Point(-11, 617)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(955, 42)
        Me.Label21.TabIndex = 17
        Me.Label21.Text = "Press next to continue"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox7
        '
        Me.PictureBox7.Image = Global.Rational_Numbers_CAI.My.Resources.Resources.Problem_4_Step_4_2
        Me.PictureBox7.Location = New System.Drawing.Point(10, 299)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(934, 127)
        Me.PictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox7.TabIndex = 15
        Me.PictureBox7.TabStop = False
        '
        'Label19
        '
        Me.Label19.Location = New System.Drawing.Point(-4, 254)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(955, 42)
        Me.Label19.TabIndex = 14
        Me.Label19.Text = "We get"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox6
        '
        Me.PictureBox6.Image = Global.Rational_Numbers_CAI.My.Resources.Resources.Problem_4_Step_4
        Me.PictureBox6.Location = New System.Drawing.Point(10, 129)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(938, 122)
        Me.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox6.TabIndex = 13
        Me.PictureBox6.TabStop = False
        '
        'Label18
        '
        Me.Label18.Location = New System.Drawing.Point(0, 84)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(955, 42)
        Me.Label18.TabIndex = 4
        Me.Label18.Text = "After adding this"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label17
        '
        Me.Label17.Location = New System.Drawing.Point(0, 42)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(955, 42)
        Me.Label17.TabIndex = 3
        Me.Label17.Text = "This is the step where we are going to add all terms and put them together"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label16
        '
        Me.Label16.Location = New System.Drawing.Point(-4, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(955, 42)
        Me.Label16.TabIndex = 2
        Me.Label16.Text = "Step 4: Put the entire numerator over denominator "
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TabPage7
        '
        Me.TabPage7.BackColor = System.Drawing.Color.DimGray
        Me.TabPage7.Controls.Add(Me.PictureBox19)
        Me.TabPage7.Controls.Add(Me.Label29)
        Me.TabPage7.Controls.Add(Me.Label28)
        Me.TabPage7.Controls.Add(Me.Label27)
        Me.TabPage7.Controls.Add(Me.Label26)
        Me.TabPage7.Controls.Add(Me.PictureBox9)
        Me.TabPage7.Controls.Add(Me.Label25)
        Me.TabPage7.Controls.Add(Me.Label24)
        Me.TabPage7.Controls.Add(Me.Label23)
        Me.TabPage7.Controls.Add(Me.Label22)
        Me.TabPage7.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 19.8!)
        Me.TabPage7.Location = New System.Drawing.Point(4, 38)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Size = New System.Drawing.Size(951, 669)
        Me.TabPage7.TabIndex = 7
        Me.TabPage7.Text = "Step 5"
        '
        'PictureBox19
        '
        Me.PictureBox19.Image = Global.Rational_Numbers_CAI.My.Resources.Resources.Problem_4_Step_4_3
        Me.PictureBox19.Location = New System.Drawing.Point(291, 401)
        Me.PictureBox19.Name = "PictureBox19"
        Me.PictureBox19.Size = New System.Drawing.Size(406, 129)
        Me.PictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox19.TabIndex = 23
        Me.PictureBox19.TabStop = False
        '
        'Label29
        '
        Me.Label29.Location = New System.Drawing.Point(0, 569)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(955, 42)
        Me.Label29.TabIndex = 22
        Me.Label29.Text = "It can now serve as our final answer"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label28
        '
        Me.Label28.Location = New System.Drawing.Point(0, 527)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(955, 42)
        Me.Label28.TabIndex = 21
        Me.Label28.Text = "This example cannot be further simplified or factored"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label27
        '
        Me.Label27.Location = New System.Drawing.Point(-4, 367)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(955, 42)
        Me.Label27.TabIndex = 19
        Me.Label27.Text = "After adding the expression above we get"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label26
        '
        Me.Label26.ForeColor = System.Drawing.Color.LightCoral
        Me.Label26.Location = New System.Drawing.Point(0, 617)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(955, 42)
        Me.Label26.TabIndex = 18
        Me.Label26.Text = "Press next to continue"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox9
        '
        Me.PictureBox9.Image = Global.Rational_Numbers_CAI.My.Resources.Resources.Problem_4_Step_4_2
        Me.PictureBox9.Location = New System.Drawing.Point(29, 209)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(903, 145)
        Me.PictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox9.TabIndex = 14
        Me.PictureBox9.TabStop = False
        '
        'Label25
        '
        Me.Label25.Location = New System.Drawing.Point(0, 164)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(955, 42)
        Me.Label25.TabIndex = 6
        Me.Label25.Text = "Remember that we don't need to FOIL the denominator yet"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label24
        '
        Me.Label24.Location = New System.Drawing.Point(0, 104)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(955, 42)
        Me.Label24.TabIndex = 5
        Me.Label24.Text = "In the numerator"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label23
        '
        Me.Label23.Location = New System.Drawing.Point(0, 62)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(955, 42)
        Me.Label23.TabIndex = 4
        Me.Label23.Text = "This step is very simple you just need to add everything up"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label22
        '
        Me.Label22.Location = New System.Drawing.Point(0, 0)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(955, 42)
        Me.Label22.TabIndex = 3
        Me.Label22.Text = "Step 5: Simplify the Numerator"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TabPage10
        '
        Me.TabPage10.BackColor = System.Drawing.Color.DimGray
        Me.TabPage10.Controls.Add(Me.PictureBox15)
        Me.TabPage10.Controls.Add(Me.PictureBox14)
        Me.TabPage10.Controls.Add(Me.PictureBox13)
        Me.TabPage10.Controls.Add(Me.PictureBox12)
        Me.TabPage10.Controls.Add(Me.PictureBox11)
        Me.TabPage10.Controls.Add(Me.Button1)
        Me.TabPage10.Controls.Add(Me.RadioButton4)
        Me.TabPage10.Controls.Add(Me.RadioButton3)
        Me.TabPage10.Controls.Add(Me.RadioButton2)
        Me.TabPage10.Controls.Add(Me.RadioButton1)
        Me.TabPage10.Controls.Add(Me.Label33)
        Me.TabPage10.Controls.Add(Me.Label32)
        Me.TabPage10.Controls.Add(Me.Label31)
        Me.TabPage10.Controls.Add(Me.Label30)
        Me.TabPage10.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 19.8!)
        Me.TabPage10.Location = New System.Drawing.Point(4, 38)
        Me.TabPage10.Name = "TabPage10"
        Me.TabPage10.Size = New System.Drawing.Size(951, 669)
        Me.TabPage10.TabIndex = 10
        Me.TabPage10.Text = "Assesment"
        '
        'PictureBox15
        '
        Me.PictureBox15.Image = Global.Rational_Numbers_CAI.My.Resources.Resources.prob12
        Me.PictureBox15.Location = New System.Drawing.Point(292, 459)
        Me.PictureBox15.Name = "PictureBox15"
        Me.PictureBox15.Size = New System.Drawing.Size(222, 65)
        Me.PictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox15.TabIndex = 19
        Me.PictureBox15.TabStop = False
        '
        'PictureBox14
        '
        Me.PictureBox14.Image = Global.Rational_Numbers_CAI.My.Resources.Resources.prob12
        Me.PictureBox14.Location = New System.Drawing.Point(21, 459)
        Me.PictureBox14.Name = "PictureBox14"
        Me.PictureBox14.Size = New System.Drawing.Size(222, 65)
        Me.PictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox14.TabIndex = 18
        Me.PictureBox14.TabStop = False
        '
        'PictureBox13
        '
        Me.PictureBox13.Image = Global.Rational_Numbers_CAI.My.Resources.Resources.prob12
        Me.PictureBox13.Location = New System.Drawing.Point(292, 358)
        Me.PictureBox13.Name = "PictureBox13"
        Me.PictureBox13.Size = New System.Drawing.Size(222, 59)
        Me.PictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox13.TabIndex = 17
        Me.PictureBox13.TabStop = False
        '
        'PictureBox12
        '
        Me.PictureBox12.Image = Global.Rational_Numbers_CAI.My.Resources.Resources.prob12
        Me.PictureBox12.Location = New System.Drawing.Point(21, 358)
        Me.PictureBox12.Name = "PictureBox12"
        Me.PictureBox12.Size = New System.Drawing.Size(222, 65)
        Me.PictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox12.TabIndex = 16
        Me.PictureBox12.TabStop = False
        '
        'PictureBox11
        '
        Me.PictureBox11.Image = Global.Rational_Numbers_CAI.My.Resources.Resources.prob12
        Me.PictureBox11.Location = New System.Drawing.Point(347, 197)
        Me.PictureBox11.Name = "PictureBox11"
        Me.PictureBox11.Size = New System.Drawing.Size(237, 64)
        Me.PictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox11.TabIndex = 15
        Me.PictureBox11.TabStop = False
        '
        'Button1
        '
        Me.Button1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Button1.Location = New System.Drawing.Point(547, 478)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 46)
        Me.Button1.TabIndex = 12
        Me.Button1.Text = "Go!"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'RadioButton4
        '
        Me.RadioButton4.AutoSize = True
        Me.RadioButton4.Location = New System.Drawing.Point(292, 423)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(148, 42)
        Me.RadioButton4.TabIndex = 11
        Me.RadioButton4.Text = "Answer 4"
        Me.RadioButton4.UseVisualStyleBackColor = True
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.Location = New System.Drawing.Point(292, 316)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(148, 42)
        Me.RadioButton3.TabIndex = 10
        Me.RadioButton3.Text = "Answer 3"
        Me.RadioButton3.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(21, 423)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(148, 42)
        Me.RadioButton2.TabIndex = 9
        Me.RadioButton2.Text = "Answer 2"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Checked = True
        Me.RadioButton1.Location = New System.Drawing.Point(21, 316)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(148, 42)
        Me.RadioButton1.TabIndex = 8
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Answer 1"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'Label33
        '
        Me.Label33.Location = New System.Drawing.Point(-1, 152)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(955, 42)
        Me.Label33.TabIndex = 7
        Me.Label33.Text = "Add the following rational expressions"
        Me.Label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label32
        '
        Me.Label32.Location = New System.Drawing.Point(-1, 115)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(955, 42)
        Me.Label32.TabIndex = 6
        Me.Label32.Text = "Feel free to look at the previous steps for reference"
        Me.Label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label31
        '
        Me.Label31.Location = New System.Drawing.Point(0, 81)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(955, 42)
        Me.Label31.TabIndex = 5
        Me.Label31.Text = "It is now time to test your skills"
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label30
        '
        Me.Label30.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.ForeColor = System.Drawing.Color.Yellow
        Me.Label30.Location = New System.Drawing.Point(-1, 10)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(955, 81)
        Me.Label30.TabIndex = 4
        Me.Label30.Text = "Wow! You have done great!"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Master_Tutorial
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 29.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.Rational_Numbers_CAI.My.Resources.Resources.blackboard
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1283, 759)
        Me.Controls.Add(Me.Einstein)
        Me.Controls.Add(Me.PreviousButton)
        Me.Controls.Add(Me.NextButton)
        Me.Controls.Add(Me.TabControl1)
        Me.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 13.8!)
        Me.ForeColor = System.Drawing.Color.White
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "Master_Tutorial"
        Me.Text = "Master Tutorial"
        CType(Me.Einstein, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage6.ResumeLayout(False)
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage7.ResumeLayout(False)
        CType(Me.PictureBox19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage10.ResumeLayout(False)
        Me.TabPage10.PerformLayout()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Einstein As System.Windows.Forms.PictureBox
    Friend WithEvents PreviousButton As System.Windows.Forms.Button
    Friend WithEvents NextButton As System.Windows.Forms.Button
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents PictureBox16 As System.Windows.Forms.PictureBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Splitter1 As System.Windows.Forms.Splitter
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents PictureBox10 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents PictureBox17 As System.Windows.Forms.PictureBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents TabPage7 As System.Windows.Forms.TabPage
    Friend WithEvents PictureBox19 As System.Windows.Forms.PictureBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents PictureBox9 As System.Windows.Forms.PictureBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents TabPage10 As System.Windows.Forms.TabPage
    Friend WithEvents PictureBox15 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox14 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox13 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox12 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox11 As System.Windows.Forms.PictureBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents RadioButton4 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
