﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CAIMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.LearnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddtitionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MediumToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HardToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MasterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PosttestToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewResultToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.MenuStrip1.Font = New System.Drawing.Font("Showcard Gothic", 19.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LearnToolStripMenuItem, Me.AddtitionToolStripMenuItem, Me.MediumToolStripMenuItem, Me.HardToolStripMenuItem, Me.MasterToolStripMenuItem, Me.PosttestToolStripMenuItem, Me.ExitToolStripMenuItem, Me.ViewResultToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1248, 50)
        Me.MenuStrip1.TabIndex = 1
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'LearnToolStripMenuItem
        '
        Me.LearnToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.LearnToolStripMenuItem.Name = "LearnToolStripMenuItem"
        Me.LearnToolStripMenuItem.Size = New System.Drawing.Size(129, 46)
        Me.LearnToolStripMenuItem.Text = "Learn"
        '
        'AddtitionToolStripMenuItem
        '
        Me.AddtitionToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.AddtitionToolStripMenuItem.Name = "AddtitionToolStripMenuItem"
        Me.AddtitionToolStripMenuItem.Size = New System.Drawing.Size(109, 46)
        Me.AddtitionToolStripMenuItem.Text = "Easy"
        '
        'MediumToolStripMenuItem
        '
        Me.MediumToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.MediumToolStripMenuItem.Name = "MediumToolStripMenuItem"
        Me.MediumToolStripMenuItem.Size = New System.Drawing.Size(166, 46)
        Me.MediumToolStripMenuItem.Text = "Medium"
        '
        'HardToolStripMenuItem
        '
        Me.HardToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.HardToolStripMenuItem.Name = "HardToolStripMenuItem"
        Me.HardToolStripMenuItem.Size = New System.Drawing.Size(123, 46)
        Me.HardToolStripMenuItem.Text = "Hard"
        '
        'MasterToolStripMenuItem
        '
        Me.MasterToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.MasterToolStripMenuItem.Name = "MasterToolStripMenuItem"
        Me.MasterToolStripMenuItem.Size = New System.Drawing.Size(157, 46)
        Me.MasterToolStripMenuItem.Text = "Master"
        '
        'PosttestToolStripMenuItem
        '
        Me.PosttestToolStripMenuItem.ForeColor = System.Drawing.Color.Gold
        Me.PosttestToolStripMenuItem.Name = "PosttestToolStripMenuItem"
        Me.PosttestToolStripMenuItem.Size = New System.Drawing.Size(201, 46)
        Me.PosttestToolStripMenuItem.Text = "Post-test"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(105, 46)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'ViewResultToolStripMenuItem
        '
        Me.ViewResultToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.ViewResultToolStripMenuItem.Name = "ViewResultToolStripMenuItem"
        Me.ViewResultToolStripMenuItem.Size = New System.Drawing.Size(243, 46)
        Me.ViewResultToolStripMenuItem.Text = "View Result"
        Me.ViewResultToolStripMenuItem.Visible = False
        '
        'CAIMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(1248, 739)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "CAIMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Rational Expressions"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents LearnToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddtitionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MediumToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HardToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MasterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PosttestToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewResultToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
