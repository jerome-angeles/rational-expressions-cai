﻿Public Class Splash

    Private Sub LearnButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LearnButton.Click
        CAIMain.Show()
        CAIMain.PreTestScores = NumericUpDown1.Value
        CAIMain.StudentName = Surname.Text + ", " + GivenName.Text + " " + MiddleName.Text(0) + "."
        Me.Hide()

    End Sub

    Private Sub BackButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BackButton1.Click
        Me.Close()
    End Sub
End Class
