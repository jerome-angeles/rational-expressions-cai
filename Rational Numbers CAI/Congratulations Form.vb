﻿Public Class Congratulations_Form

    Public Sub calculate()
        Label2.Text = "You've scored " + CAIMain.PostTestScores.ToString + "/10"
        Dim increase As Integer = CAIMain.PostTestScores - CAIMain.PreTestScores
        Dim percentIncrease As Integer
        Try
            percentIncrease = increase / CAIMain.PreTestScores * 100
        Catch ex As Exception

        End Try

        Label4.Text = percentIncrease.ToString() + "% Improvement"
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        CAIMain.ViewResultToolStripMenuItem.Visible = True
        Me.Hide()
    End Sub

    Private Sub Congratulations_Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        MdiParent = CAIMain
    End Sub
End Class