﻿Imports Rational_Numbers_CAI.My.Resources

Public Class Questions

    Public Property toExit As Boolean = True
    Public Property askedQuestion As QuestionClass
    Public Property questionOrder As List(Of Int32)
    Public Property questions As List(Of QuestionClass)

    Public Property numberOrder As Integer = 0

    Public Property q1 As New QuestionClass(CorrectAnswer.A, _1Problem, _1a, _1b, _1c, _1d)
    Public Property q2 As New QuestionClass(CorrectAnswer.A, _2Problem, _2a, _2b, _2c, _2d)
    Public Property q3 As New QuestionClass(CorrectAnswer.A, _3Problem, _3a, _3b, _3c, _3d)
    Public Property q4 As New QuestionClass(CorrectAnswer.A, _4Problem, _4a, _4b, _4c, _4d)
    Public Property q5 As New QuestionClass(CorrectAnswer.A, _5Problem, _5a, _5b, _5c, _5d)
    Public Property q6 As New QuestionClass(CorrectAnswer.A, _6Problem, _6a, _6b, _6c, _6d)
    Public Property q7 As New QuestionClass(CorrectAnswer.A, _7Problem, _7a, _7b, _7c, _7d)
    Public Property q8 As New QuestionClass(CorrectAnswer.A, _8Problem, _8a, _8b, _8c, _8d)
    Public Property q9 As New QuestionClass(CorrectAnswer.A, _9Problem, _9a, _9b, _9c, _9d)
    Public Property q10 As New QuestionClass(CorrectAnswer.A, _10Problem, _10a, _10b, _10c, _10d)
    Public Property currentCorrectAnswer As CorrectAnswer


    Private Sub Questions_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        questionOrder = GenerateRandomNumbers()

        CAIMain.PostTestScores = 0
        questions = New List(Of QuestionClass)
        q1 = New QuestionClass(CorrectAnswer.A, _1Problem, _1a, _1b, _1c, _1d)
        q2 = New QuestionClass(CorrectAnswer.A, _2Problem, _2a, _2b, _2c, _2d)
        q3 = New QuestionClass(CorrectAnswer.A, _3Problem, _3a, _3b, _3c, _3d)
        q4 = New QuestionClass(CorrectAnswer.A, _4Problem, _4a, _4b, _4c, _4d)
        q5 = New QuestionClass(CorrectAnswer.A, _5Problem, _5a, _5b, _5c, _5d)
        q6 = New QuestionClass(CorrectAnswer.A, _6Problem, _6a, _6b, _6c, _6d)
        q7 = New QuestionClass(CorrectAnswer.A, _7Problem, _7a, _7b, _7c, _7d)
        q8 = New QuestionClass(CorrectAnswer.A, _8Problem, _8a, _8b, _8c, _8d)
        q9 = New QuestionClass(CorrectAnswer.A, _9Problem, _9a, _9b, _9c, _9d)
        q10 = New QuestionClass(CorrectAnswer.A, _10Problem, _10a, _10b, _10c, _10d)
        questions.Add(q1)
        questions.Add(q2)
        questions.Add(q3)
        questions.Add(q4)
        questions.Add(q5)
        questions.Add(q6)
        questions.Add(q7)
        questions.Add(q8)
        questions.Add(q9)
        questions.Add(q10)
        AskNextQuestion()
    End Sub

    Private Sub EnableExit(ByVal sender As System.Object, ByVal e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.K Then
            toExit = False
        End If
    End Sub

    Public Sub AskNextQuestion()
        If numberOrder = questions.Count Then
            finish()
        Else
            AskQuestion(questions(questionOrder(numberOrder)))
            numberOrder = numberOrder + 1
        End If
    End Sub

    Public Sub AskQuestion(ByVal question As QuestionClass)
        problem.Image = question.problemImage
        picturea.Image = question.a
        pictureb.Image = question.b
        picturec.Image = question.c
        pictured.Image = question.d
        currentCorrectAnswer = question.correctAnswer
        askedQuestion = question
    End Sub

    Private Function GenerateRandomNumbers() As List(Of Integer)
        Dim rand As New Random
        Dim result As New List(Of Integer)
        Dim randomNumber As Integer
        Dim counter As Integer = 1
        While True
            randomNumber = rand.Next(0, 10)
            If Not result.Contains(randomNumber) Then
                result.Add(randomNumber)
                counter = counter + 1
            End If
            If Not counter <= 10 Then
                Return result
            End If
        End While
        Return result
    End Function

    Private Sub frminstituteselect_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        e.Cancel = toExit
        If Not toExit Then
            CAIMain.MainMenuStrip.Enabled = True
            CAIMain.ExitToolStripMenuItem.Enabled = True
            CAIMain.PostTestScores = 0
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If RadioButton1.Checked And askedQuestion.correctAnswer = CorrectAnswer.A Then
            correct()
        End If
        If RadioButton2.Checked And askedQuestion.correctAnswer = CorrectAnswer.B Then
            correct()
        End If
        If RadioButton3.Checked And askedQuestion.correctAnswer = CorrectAnswer.C Then
            correct()
        End If
        If RadioButton4.Checked And askedQuestion.correctAnswer = CorrectAnswer.D Then
            correct()
        End If
        AskNextQuestion()
    End Sub

    Private Sub correct()
        CAIMain.PostTestScores = CAIMain.PostTestScores + 1
    End Sub

    Private Sub finish()
        toExit = False
        CAIMain.MainMenuStrip.Enabled = True
        CAIMain.ExitToolStripMenuItem.Enabled = True
        Congratulations_Form.Show()
        Congratulations_Form.calculate()
        Me.Close()
    End Sub

End Class

Public Class QuestionClass
    Public Property correctAnswer As CorrectAnswer
    Public Property problemImage As Image
    Public Property a As Image
    Public Property b As Image
    Public Property c As Image
    Public Property d As Image

    Sub New(ByVal correct As CorrectAnswer, ByVal image As Image, ByVal _a As Image, ByVal _b As Image, ByVal _c As Image, ByVal _d As Image)
        correctAnswer = correct
        problemImage = image
        a = _a
        b = _b
        c = _c
        d = _d
    End Sub
End Class

Public Enum CorrectAnswer
    A
    B
    C
    D
End Enum