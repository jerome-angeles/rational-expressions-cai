﻿Public Class Medium_Tutorial
    Dim nextCounter As Integer = 0
    Dim correctAnswer As String = "Answer 4"

    Private Sub OnTabChangeIndex(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabControl1.SelectedIndexChanged
        'MessageBox.Show(TabControl1.SelectedIndex)

        If (TabControl1.SelectedIndex = TabControl1.TabCount - 1) Then
            NextButton.Enabled = False
        Else
            NextButton.Enabled = True
        End If
        If (TabControl1.SelectedIndex = 0) Then
            PreviousButton.Enabled = False
        Else
            PreviousButton.Enabled = True
        End If
    End Sub


    Private Sub NextButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NextButton.Click
        TabControl1.SelectTab(TabControl1.SelectedIndex + 1)
        'If (TabControl1.SelectedIndex = TabControl1.TabCount - 1) Then
        '    NextButton.Enabled = False
        'End If
        'PreviousButton.Enabled = True

    End Sub

    Private Sub Addition_Easy_Tutorial_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        PreviousButton.Enabled = False
    End Sub

    Private Sub PreviousButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PreviousButton.Click
        TabControl1.SelectTab(TabControl1.SelectedIndex - 1)
        'If (TabControl1.SelectedIndex = TabControl1.TabCount + 1) Then
        '    PreviousButton.Enabled = False
        'End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If RadioButton1.Checked And RadioButton1.Text = correctAnswer Then
            RightAnswer()
        ElseIf RadioButton1.Checked And Not RadioButton1.Text = correctAnswer Then
            WrongAnswer()
        End If
        If RadioButton2.Checked And RadioButton2.Text = correctAnswer Then
            RightAnswer()
        ElseIf RadioButton2.Checked And Not RadioButton2.Text = correctAnswer Then
            WrongAnswer()
        End If
        If RadioButton3.Checked And RadioButton3.Text = correctAnswer Then
            RightAnswer()
        ElseIf RadioButton3.Checked And Not RadioButton3.Text = correctAnswer Then
            WrongAnswer()
        End If
        If RadioButton4.Checked And RadioButton4.Text = correctAnswer Then
            RightAnswer()
        ElseIf RadioButton4.Checked And Not RadioButton4.Text = correctAnswer Then
            WrongAnswer()
        End If
        Me.Close()

    End Sub

    Private Sub WrongAnswer()
        MessageBox.Show("Wrong Answer", "Please try again!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    End Sub

    Private Sub RightAnswer()
        MessageBox.Show("You are correct!", "Proceed to the next problem!", MessageBoxButtons.OK, MessageBoxIcon.Question)
    End Sub
End Class